package cn.zhucongqi.tiger.controllers;

import cn.zhucongqi.tiger.services.AppSecretKeysService;
import cn.zhucongqi.tiger.services.CodeDataService;
import cn.zhucongqi.tiger.validators.AppSecretKeysValidator;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.ext2.core.ControllerExt;

/**
 * 
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class TigerMobileController extends ControllerExt {
	
	private AppSecretKeysService appSecretKeysService;
	private CodeDataService codeDataService;
	
	/**
	 * 授权
	 */
	@Before({POST.class, AppSecretKeysValidator.class})
	public void authDevice() {
		String uuid = this.getPara("uuid");
		String appkey = this.getPara("appkey");
		String bundle = this.getPara("bundle");
		appSecretKeysService.authDevice(uuid, appkey, bundle);
	}
	
	/**
	 * 记录数据
	 */
	 public void logData(){
		 String code = this.getPara("code");
		 String token = this.getPara("token");
		 String jsondata = this.getPara("data");
		 codeDataService.logData(token, code, jsondata);
	 }
}
